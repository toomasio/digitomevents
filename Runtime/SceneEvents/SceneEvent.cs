﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomEvents
{
    public abstract class SceneEvent : ScriptableObject
    {
        public abstract void DoEvent(GameObject _sender, GameObject _receiver);
    }
}


