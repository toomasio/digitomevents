﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomUtilities;

namespace DigitomEvents
{
    [System.Serializable]
    public class SceneEventContainer
    {
        [ExpandableObject] public SceneEvent[] sceneEvents;

        public void DoEvents(GameObject _sender, GameObject _receiver)
        {
            for (int i = 0; i < sceneEvents.Length; i++)
            {
                sceneEvents[i].DoEvent(_sender, _receiver);
            }
        }
    }
}


