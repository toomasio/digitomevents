﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomEvents
{
    public class GOEUnityEvents : GameObjectEvent
    {
        [SerializeField] private UnityEventContainer unityEvents;

        protected override void DoAffectedObjectEvent(GameObject affectedObject)
        {
            unityEvents.unityEvents?.Invoke();
        }
    }
}


