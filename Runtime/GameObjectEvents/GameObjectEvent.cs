﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomUtilities;

namespace DigitomEvents
{
    [RequireComponent(typeof(MonoGameObjectEvents))]
    public abstract class GameObjectEvent : MonoBehaviour
    {
        [SerializeReference, SelectableSubType] protected GetGameObjectForEvent affectedObject;

        protected GameObject sender;
        protected GameObject receiver;

        public virtual void DoEvent(GameObject _sender, GameObject _receiver)
        {
            sender = _sender;
            receiver = _receiver;
            var obj = affectedObject.GetGameObject(_sender, _receiver);
            DoAffectedObjectEvent(obj);
        }

        protected abstract void DoAffectedObjectEvent(GameObject affectedObject);
    }
}