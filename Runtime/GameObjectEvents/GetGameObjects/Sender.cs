﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomEvents
{
    [System.Serializable]
    public class Sender : GetGameObjectForEvent
    {
        public override GameObject GetGameObject(GameObject sender, GameObject receiver) 
        {
            return sender;
        }
    }
}


