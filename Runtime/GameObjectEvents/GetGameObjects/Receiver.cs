﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomEvents
{
    [System.Serializable]
    public class Receiver : GetGameObjectForEvent
    {
        public override GameObject GetGameObject(GameObject sender, GameObject receiver) 
        {
            return receiver;
        }
    }
}


