﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomEvents
{
    [System.Serializable]
    public class GetGameObjectForEvent
    {
        public virtual GameObject GetGameObject(GameObject sender, GameObject receiver) 
        {
            return null;
        }
    }
}


