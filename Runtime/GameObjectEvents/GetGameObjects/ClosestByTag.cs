﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomUtilities;

namespace DigitomEvents
{
    [System.Serializable]
    public class ClosestByTag : GetGameObjectForEvent
    {
        [SerializeField] private string tag;

        public override GameObject GetGameObject(GameObject sender, GameObject receiver) 
        {
            return GameObjectExtensions.FindClosestByTag(sender.transform.position, tag);
        }
    }
}


