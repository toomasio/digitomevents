﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomEvents
{
    public class GOEDestroy : GameObjectEvent
    {
        [SerializeField] private float delay;

        protected override void DoAffectedObjectEvent(GameObject affectedObject)
        {
            Destroy(affectedObject, delay);
        }
    }
}


