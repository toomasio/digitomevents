﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    [DisallowMultipleComponent]
    public class OnTriggerStayGameObjectEvents : MonoGameObjectEvents
    {
        private void OnTriggerStay(Collider other)
        {
            OnGameObjectEvents(gameObject, other.gameObject);
        }

    }
}