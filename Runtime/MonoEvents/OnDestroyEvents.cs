﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    public class OnDestroyEvents : MonoUnityEvents
    {
        private void OnDestroy()
        {
            DoUnityEvents();
        }

    }
}