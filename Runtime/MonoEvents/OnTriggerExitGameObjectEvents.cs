﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    [DisallowMultipleComponent]
    public class OnTriggerExitGameObjectEvents : MonoGameObjectEvents
    {
        private void OnTriggerExit(Collider other)
        {
            OnGameObjectEvents(gameObject, other.gameObject);
        }

    }
}