using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    [DisallowMultipleComponent]
    public class OnDisableGameObjectEvents : MonoGameObjectEvents
    {
        private void OnDisable() => OnGameObjectEvents(gameObject, gameObject);
    }
}