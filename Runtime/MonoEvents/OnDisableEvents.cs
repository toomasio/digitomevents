﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    public class OnDisableEvents : MonoBehaviour
    {
        public event Action onDisable;
        void OnDisable()
        {
            onDisable?.Invoke();
        }

    }
}