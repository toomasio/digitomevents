﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    [DisallowMultipleComponent]
    public class OnTriggerEnterGameObjectEvents : MonoGameObjectEvents
    {
        private void OnTriggerEnter(Collider other)
        {
            OnGameObjectEvents(gameObject, other.gameObject);
        }
    }
}