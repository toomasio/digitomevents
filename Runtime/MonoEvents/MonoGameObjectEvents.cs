using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    public abstract class MonoGameObjectEvents : MonoBehaviour
    {
        [SerializeField] protected GameObjectEvent[] events = null;

        protected virtual void OnGameObjectEvents(GameObject sender, GameObject receiver)
        {
            for (int i = 0; i < events.Length; i++)
            {
                events[i].DoEvent(sender, receiver);
            }
        }

    }
}