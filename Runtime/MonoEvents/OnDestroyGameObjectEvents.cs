using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    [DisallowMultipleComponent]
    public class OnDestroyGameObjectEvents : MonoGameObjectEvents
    {
        private void OnDestroy() => OnGameObjectEvents(gameObject, gameObject);
    }
}