using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    public abstract class MonoTriggerEventsUnityEvents : MonoTriggerEvents
    {
        [SerializeField] protected UnityEventContainer events = null;

        protected virtual void DoUnityEvents(Collider col)
        {
            if (!TagValid(col)) return;

            events.unityEvents?.Invoke();
        }

        protected bool TagValid(Collider col)
        {
            if (!onlyAllowTags) return true; 
            bool valid = false;
            for (int i = 0; i < allowTags.Length; i++)
            {
                if (col.CompareTag(allowTags[i]))
                    valid = true;
            }
            return valid;
        }

    }
}