using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    public class OnAwakeGameObjectEvents : MonoGameObjectEvents
    {
        private void Awake()
        {
            OnGameObjectEvents(gameObject, gameObject);
        }
    }
}