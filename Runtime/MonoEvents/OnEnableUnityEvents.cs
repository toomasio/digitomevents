﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    public class OnEnableUnityEvents : MonoBehaviour
    {
        [SerializeField] private UnityEventContainer onEnableEvents;

        void OnEnable()
        {
            onEnableEvents.unityEvents.Invoke();
        }

    }
}