﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    public class OnTriggerExitUnityEvents : MonoTriggerEventsUnityEvents
    {
        private void OnTriggerExit(Collider other)
        {
            DoUnityEvents(other);
        }

    }
}