﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    public class OnTriggerStayEvents : MonoTriggerEventsUnityEvents
    {
        private void OnTriggerStay(Collider other)
        {
            DoUnityEvents(other);
        }

    }
}