using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    [DisallowMultipleComponent]
    public class OnEnableGameObjectEvents : MonoGameObjectEvents
    {
        private void OnEnable() => OnGameObjectEvents(gameObject, gameObject);
    }
}