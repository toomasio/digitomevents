﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    public class OnDisableUnityEvents : MonoBehaviour
    {
        [SerializeField] private UnityEventContainer onDisableEvents;

        void OnDisable()
        {
            onDisableEvents.unityEvents.Invoke();
        }

    }
}