using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    public abstract class MonoUnityEvents : MonoBehaviour
    {
        [SerializeField] protected UnityEventContainer events = null;

        protected virtual void DoUnityEvents()
        {
            events.unityEvents?.Invoke();
        }

    }
}