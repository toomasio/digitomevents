﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    public class OnStartUnityEvents : MonoBehaviour
    {
        [SerializeField] private UnityEventContainer onStartEvents;

        private void Start()
        {
            onStartEvents.unityEvents.Invoke();
        }

    }
}