﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomUtilities;

namespace DigitomEvents
{
    public class OnStartEvents : MonoBehaviour
    {
        [SerializeField] private UnityEventContainer onStartUnityEvents = null;
        [SerializeField] private CallMethod[] onStartMethods = null;

        void Start()
        {
            onStartUnityEvents.unityEvents?.Invoke();
            for (int i = 0; i < onStartMethods.Length; i++)
            {
                onStartMethods[i].Invoke();
            }
        }

        
    }
}


