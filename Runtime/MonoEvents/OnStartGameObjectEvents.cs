using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    [DisallowMultipleComponent]
    public class OnStartGameObjectEvents : MonoGameObjectEvents
    {
        private void Start()
        {
            OnGameObjectEvents(gameObject, gameObject);
        }
    }
}