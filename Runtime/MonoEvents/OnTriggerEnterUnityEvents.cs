﻿using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    public class OnTriggerEnterUnityEvents : MonoTriggerEventsUnityEvents
    {
        private void OnTriggerEnter(Collider other)
        {
            DoUnityEvents(other);
        }

    }
}