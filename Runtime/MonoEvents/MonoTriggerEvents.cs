using System.Collections;
using System.Collections.Generic;
using DigitomUtilities;
using UnityEngine;

namespace DigitomEvents
{
    public abstract class MonoTriggerEvents : MonoBehaviour
    {
        [SerializeField] protected bool onlyAllowTags;
        [SerializeField, TagSelect] protected string[] allowTags = null;
    }
}