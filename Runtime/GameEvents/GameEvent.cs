﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomEvents
{
    [CreateAssetMenu(menuName = "Ngn/Game Event")]
    public class GameEvent : ScriptableObject
    {
        public event System.Action OnEventTriggered;

        public void TriggerEvent()
        {
            OnEventTriggered?.Invoke();
        }
    }

}

