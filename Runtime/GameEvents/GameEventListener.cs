﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomEvents
{
    public class GameEventListener : MonoBehaviour
    {
        [SerializeField] protected GameEvent gameEvent;
        [SerializeField] protected UnityEventContainer onEventTriggeredEvents;

        protected virtual void OnEnable()
        {
            gameEvent.OnEventTriggered += OnEventTriggered;
        }

        protected virtual void OnDisable()
        {
            gameEvent.OnEventTriggered -= OnEventTriggered;
        }

        protected virtual void OnEventTriggered()
        {
            onEventTriggeredEvents.unityEvents?.Invoke();
        }
    }

}

