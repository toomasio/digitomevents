﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DigitomEvents
{
    [System.Serializable]
    public class UnityEventContainer
    {
        public UnityEvent unityEvents;
    }
}


